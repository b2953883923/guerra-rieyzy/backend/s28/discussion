// [SECTION] Non-Mutator Methods 
	/*
		- Non-mutator methods are functions that do not modify or change an array after they are created.
		-These methods do not manipulate the original array performing various such as returning elements from an array and combining array and printing the output.
	*/ 

	let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

	// indexOf()

		/*
			- Returns the index number of the first matching element found in an array
			- if no match is found, the result will be -1.
			-left to right of checking.

			Syntax:

				arrayName.indexOf(searchValue);
				arrayName.indexOf(searchValue, fromIndex);
		*/

		let firstIndex = countries.indexOf('PH');
		console.log("Result of indexOf method: " + firstIndex);

		let invalidCountry = countries.indexOf('BR');
		console.log("Result of indexOf method: " + invalidCountry);

		let secondIndex = countries.indexOf('PH', 3);
		console.log("Result of indexOf method: " + secondIndex);

		// lastIndexOf()

		/*
			- Returns the index number of the first matching element found in an array.
			- The search process will be done from the last element proceeding to the first element.

			Syntax:

				arrayName.lastIndexOf(searchValue);
				arrayName.lastIndexOf(searchValue, fromIndex);
		*/ 

		let lastIndex = countries.lastIndexOf('PH'); 
		console.log("Result of lastIndexOf method: " + lastIndex);

		let invalidNation = countries.lastIndexOf('BR');
		console.log("Result of lastIndexOf method: " + invalidNation);

		let thirdIndex = countries.lastIndexOf('PH', 3);
		console.log("Result of lastIndexOf method: " + thirdIndex);

		// slice()

		/*
			- Portions/Slices elements from an array AND returns a new array.

			Syntax:
			arrayName.splice(startingIndex);
			arrayName.splice(startingIndex, endingIndex);
		*/ 

	// Slicing off element from a specified index up to the last element.

	let sliceArrayA = countries.slice(2);
	console.log("Result of slice method: ");
	console.log(sliceArrayA);

	// We still have the original element from original array
	console.log(countries)

	// Slicing of the element from a specified index to another index
	let sliceArrayB = countries.slice(2, 4);
	console.log("Result of slice method: ");
	console.log(sliceArrayB);

	// Slicing off elements from the last element of the array
	let sliceArrayC = countries.slice(-3);
	console.log("Result of slice method: ");
	console.log(sliceArrayC);

	// toString();

	/*
		- Returns an array as a string seperated by  comma (default);
		Syntax:
			arrayName.toString();
	*/

	let stringArray = countries.toString();
	console.log("Result of toString method: ");
	console.log(stringArray);

	// concat()

		/*
			- Combines two arrays and returns the combined result.

			Syntax:
				arrayA.concat(B)
				arrayA.concat(ElementA);
		*/ 

		let taskArrayA = ["drink html", "eat javascript"];
		let taskArrayB = ["inhale css", "breathe sass"];
		let taskArrayC = ["get git", "play node"];

		let tasks = taskArrayA.concat(taskArrayB);
		console.log("Result of concat method: ");
		console.log(tasks);

		//Combining all task
		let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
		console.log("Result of concat method: ");
		console.log(allTasks);

		//Combining arrays with elements
		let combinedTasks = taskArrayA.concat("smell expreess", "throw react");
		console.log("Result of concat method: ");
		console.log(combinedTasks);

		// join()

		/*
				- returns an array as string separated by specified separator string.

				Syntax;
					arrayName.join('seperatorString');
		*/ 

	let users = ["John", "Jane", "Joe", "Robert"]

	console.log(users.join());
	console.log(users.join(' '));
	console.log(users.join(' - '));

	// [SECTION] Iteration Methods

	/*
		- Iteration methods are loopps designed to perform repetitive task on array. It loops in every items/elements in an array. 
		- Useful for manipulating array data resulting in complex
	*/

	// foreach()

	/*
		- similar to for loop that iterates on each element in the array.
		- it has an anonymous function that is run on each element in every iteration.
		- forEach() does not return anything.

		Syntax: 
			arrayName.forEach(function(indivElemenet){
				statement/codeblock;
			});
	*/ 

	allTasks.forEach(function(task){
		console.log(task)
	})

	// using forEach with conditional statements
	let filteredTask = []
	allTasks.forEach(function(task) {
		if(task.length > 10 ){
			// add the element to the filteredTask array.
			filteredTask.push(task);
		}
	})

	console.log("Result of filteredTask using forEach method: ")
	console.log(filteredTask);

	// map()

		/*
			- Iterates on each element AND returns new array with different values depending on the result of the function's operation.
			- unlike the forEach method, the map method required the use of the "return" statement.
			
			Syntax:
			let/const resultArray = arrayName.map(function(indivElement){
				statement/codeblock
			})
		*/
	let numbers = [1, 2, 3, 4 , 5];
	let numberMap = numbers.map(function(number){
		return number * number;

	})
	console.log("Original array: ");
	console.log(numbers);
	console.log("Rsult of map method: ");
	console.log(numberMap);

	// map() vs forEach()
	let numberForEach = numbers.forEach(function(number){
		return number * number;

	})
	console.log(numberForEach);

	// every()

	/*
		- Checks if all elements in an array meet the given condition.
		- Returns a true value if all elements meet the condition and false if otherwise.
		
		Syntax:
			let/const resultArray = arrayName.every(function(indivElement){
					return expression/statement/conditions;
			})
	*/ 

	let allValid = numbers.every(function(number){
		return(number < 3);
	})

	console.log("Result of every method: ")
	console.log(allValid);

	// some()

	/*
		- Checks if atleast one element in the array meets the given condition.
		- Returns a true value if atleast one element meets the condition and false if otherwise.

		Syntax:
			let/const resultArray = arrayName.some(function(indivElement){
					return expression/statement/conditions;
			})
	*/ 

	let someValid = numbers.some(function(number){
		return(number < 2);
	})

	console.log("Result of every method: ")
	console.log(someValid);

	// filter()

	/*
		- Returns new array that contains elements which meets the given condition
	    - Returns an empty array if no elements were found
	    - Useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods
	    - Mastery of loops can help us work effectively by reducing the amount of code we use
	    - Several array iteration methods may be used to perform the same result
	    - Syntax
	        let/const resultArray = arrayName.filter(function(indivElement) {
	            return expression/condition;
	        })

	*/

	let filterValid = numbers.filter(function(number){
		return (number < 3 ) ;
	})
	console.log("Result of filter method: ");
	console.log(filterValid);

	//if no match is found based on the condition;
	let nothingFound = numbers.filter(function(number){
		return (number == 0 ) ;
	})
	console.log("Result of filter method: ");
	console.log(nothingFound);

	//includes()

	/*
		- includes() method checks if the argument passed can be found in the array.
	    - it returns a boolean which can be saved in a variable.
	        - returns true if the argument is found in the array.
	        - returns false if it is not.
	    - Syntax:
	        arrayName.includes(<argumentToFind>)

	*/ 

	let products = ["Mouse", "Keyboard", "Monitor", "Keypad", "Laptop"];

	let productFound1 = products.includes("Mouse");
	console.log(productFound1); //return true

	let productFound2 = products.includes("Headset");
	console.log(productFound2); // return false

	// method chaining 

	let filteredProducts = products.filter(function(product){
		return product.toLowerCase().includes('a');
	});
	console.log(filteredProducts);

	//reduce()

	/*
		- Evaluates elements from left to right and returns/reduces the array into a single value
	    - Syntax
	        let/const resultArray = arrayName.reduce(function(accumulator, currentValue) {
	            return expression/operation
	        })
	    - The "accumulator" parameter in the function stores the result for every iteration of the loop
	    - The "currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
	    - How the "reduce" method works
	        1. The first/result element in the array is stored in the "accumulator" parameter
	        2. The second/next element in the array is stored in the "currentValue" parameter
	        3. An operation is performed on the two elements
	        4. The loop repeats step 1-3 until all elements have been worked on

	*/ 

	let iteration = 0;

	let reduceArray = numbers.reduce(function(x,y){
		console.warn('current iteration: ' + ++iteration);
		console.log("accumulator: " + x );
		console.log("currentValue: " + y);

		return x + y;
	})

	console.log("Result of reduce method: " + reduceArray);

	let list = ["Hello", "Again", "World"];

	let reducedJoin = list.reduce(function(x,y){
		return x + ' ' + y;
	})

	console.log("Result of reduced method: " + reducedJoin);